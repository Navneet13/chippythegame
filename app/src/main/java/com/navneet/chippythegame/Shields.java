package com.navneet.chippythegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Shields {
    private int xPosition;
    private int yPosition;
    private Bitmap image;

    private Rect hitBox;

    Shields(Context context, int xPosition, int yPosition) {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.shield);

        this.hitBox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + 50,
                this.yPosition + 50
        );
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public Rect getHitBox() {
        return hitBox;
    }

    public void setHitBox(Rect hitBox) {
        this.hitBox = hitBox;
    }
}