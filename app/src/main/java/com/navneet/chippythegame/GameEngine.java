package com.navneet.chippythegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    public enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }

    // Game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // Drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;
    private Bitmap background;

    // Screen resolution variables
    private int screenWidth;
    private int screenHeight;
    private int bottomMargin = 250;

    private int enemySpeed = 0;

    float mouseX;
    float mouseY;

    long totalTimeElapsed = 0;
    int totalScore = 1;
    int totalLives = 5;

    int coreHit = 0;

    // SPRITES
    // Array of Bullets
    ArrayList<Bullet> bulletsArrayList = new ArrayList<>();

    // Array of Enemy Chips
    ArrayList<Enemy> enemyArrayList = new ArrayList<>();

    // Array of Enemy Bullets
    ArrayList<Bullet> enemyBulletsArrayList = new ArrayList<>();

    Enemy coreEnemy;
    Player player;
    Shields shield;

    int SQUARE_WIDTH = 50;
    int ENEMY_BULLET_BASE_SPEED = 20;
    int BULLET_BASE_SPEED = 20;

    Direction enemyDirection = Direction.LEFT;

    int shieldCount = 0;
    boolean shouldShieldDisplay = false;
    boolean activateShield = false;
    int shieldActivationTime = 0;

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // initialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // Make Player
        createPlayer(context);

        // Initialize sprites(Player, Bullet, Enemy)
        // Make Bullets
        createBullets(context);

        // Make Enemy
        createEnemy(context);

        // Make Enemy Bullets
        createEnemyBullets(context);

        // Setup the background
        this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        this.background = Bitmap.createScaledBitmap(
                this.background,
                this.screenWidth,
                this.screenHeight,
                false
        );

        // Time Elapsed
//        final Handler handler = new Handler();
//        Timer timer = new Timer(false);
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        totalTimeElapsed += 1;
//                    }
//                });
//            }
//        };
//        timer.schedule(timerTask, 1000);
    }

    // Initialize Bullets List
    private void createBullets(Context context) {
        for (int i = 1; i < 5; i++) {
            Bullet b = new Bullet(context, player.getxPosition(), player.getyPosition(), SQUARE_WIDTH, BULLET_BASE_SPEED * i);
            this.bulletsArrayList.add(b);
        }
    }

    // Initialize Enemy ArrayList
    private void createEnemy(Context context) {
        int rows = 9, columns = 9;
        int x, y = 10;
        for (int i = 0; i < rows; i++) {
            x = screenWidth - 700;
            y += SQUARE_WIDTH;
            for (int j = 0; j < columns; j++) {
                x += SQUARE_WIDTH;
                Enemy enemy = new Enemy(context, x, y, SQUARE_WIDTH, 0);
                enemyArrayList.add(enemy);
            }
        }
        int middle = (rows * columns) / 2;
        this.coreEnemy = enemyArrayList.get(middle);
    }

    // Initialize Bullets List
    private void createEnemyBullets(Context context) {
        for (int i = 1; i < 5; i++) {
            Bullet b = new Bullet(context, coreEnemy.getxPosition(), coreEnemy.getyPosition(), SQUARE_WIDTH, ENEMY_BULLET_BASE_SPEED * i);
            b.setTargetX(player.getxPosition());
            b.setTargetY(player.getyPosition());
            this.enemyBulletsArrayList.add(b);
        }
    }

    // Initialize Player
    private void createPlayer(Context context) {
        player = new Player(context, 100, 100);
    }

    // Initialize Shields
    private void createShields(Context context, int xPosition, int yPosition) {
        shield = new Shields(context, xPosition, yPosition);
    }

    @Override
    public void run() {
        // @TODO: Put game loop in here
        while (gameIsRunning == true) {
            updateGame();
            redrawSprites();
            controlFPS();
        }
    }

    // Game Loop Methods
    public void updateGame() {

        shieldCount += 1;

        shouldShieldDisplay = shieldCount >= 100 && shieldCount < 1000;

        activateShield = shieldActivationTime >= 100 && shieldActivationTime < 5000;
        if (activateShield) {
            shieldActivationTime += 1;
        } else {
            shieldActivationTime = 0;
        }

        // 1. MOVE THE TARGET (Purple square)
        moveEnemy();

        // 2. MOVE YOUR BULLETS
        for (int i = 0; i < bulletsArrayList.size(); i++) {
            Bullet b = bulletsArrayList.get(i);
            moveBulletToTarget(b);
        }

        for (int i = 0; i < enemyBulletsArrayList.size(); i++) {
            Bullet b = enemyBulletsArrayList.get(i);
            moveEnemyBulletsToPlayer(b);
        }

        // 3. CREATE NEW THE BULLET
        spawnBullet();

        spawnEnemyBullets();

        // 4. COLLISION DETECTION
        collisionDetection();

        // 5. CREATE SHIELDS
        spawnShields();
        shieldPlayerCollision();
    }

    public void redrawSprites() {

        if (holder.getSurface().isValid()) {

            // Initialize the canvas
            canvas = holder.lockCanvas();

            // Set the game's background color
            canvas.drawColor(Color.argb(255, 255, 255, 255));

            // Setup stroke style and width
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(8);
            paintbrush.setStyle(Paint.Style.FILL);

            // DRAW THE BACKGROUND
            canvas.drawBitmap(this.background, 0, 0, paintbrush);

            // DRAW BULLETS
            paintbrush.setColor(Color.GREEN);
            for (int i = 0; i < this.bulletsArrayList.size(); i++) {
                Bullet b = this.bulletsArrayList.get(i);
                canvas.drawCircle(b.getxPosition(), b.getyPosition(), 20, paintbrush);
            }

            // DRAW ENEMY
            for (int i = 0; i < enemyArrayList.size(); i++) {
                Enemy enemy = this.enemyArrayList.get(i);
                paintbrush.setColor(Color.YELLOW);
                if (enemy == coreEnemy) {
                    paintbrush.setColor(Color.CYAN);
                }
                canvas.drawRect(enemy.getxPosition(), enemy.getyPosition(), enemy.getxPosition() + SQUARE_WIDTH, enemy.getyPosition() + SQUARE_WIDTH, paintbrush);
            }

            // DRAW ENEMY BULLETS
            paintbrush.setColor(Color.RED);
            for (int i = 0; i < enemyBulletsArrayList.size(); i++) {
                Bullet b = enemyBulletsArrayList.get(i);
                canvas.drawCircle(b.getxPosition(), b.getyPosition(), 20, paintbrush);
            }

            // DRAW PLAYER
            canvas.drawBitmap(player.getImage(), player.getxPosition(), player.getyPosition(), paintbrush);

            // Score
            paintbrush.setColor(Color.WHITE);
            paintbrush.setStrokeWidth(5);
            paintbrush.setTextSize(60);
            canvas.drawText("Score: " + (totalScore), screenWidth - 350, 100, paintbrush);
            if (totalLives <= 0) {
                canvas.drawText("GAME OVER!!!!", 20, 100, paintbrush);
            } else {
                canvas.drawText("Lives: " + totalLives, 20, 100, paintbrush);
            }

            // SHIELDS
            if (shield != null) {
                canvas.drawBitmap(this.shield.getImage(), shield.getxPosition(), shield.getyPosition(), paintbrush);
                shieldCount = 0;
            }
            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }
    }

    public void controlFPS() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                this.mouseX = event.getX();
                this.mouseY = event.getY();

                movePlayerToMouse(this.mouseX, this.mouseY);
                break;
        }
        return true;
    }

    // Game Status - Pause & Resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    /**
     * Movements
     */
    public void moveEnemy() {

        int horizontalEdge = screenWidth - 100;
        int verticalEdge = screenHeight - bottomMargin;
        // MOVEMENT DIRECTION
        for (int i = 0; i < enemyArrayList.size(); i++) {
            Enemy enemy = enemyArrayList.get(i);

            int xPosition = enemy.getxPosition();
            int yPosition = enemy.getyPosition();

            if (xPosition < screenWidth / 2 && yPosition <= 100) {
                enemyDirection = Direction.DOWN;
                break;
            } else if (yPosition >= verticalEdge && xPosition <= screenWidth / 2) {
                enemyDirection = Direction.RIGHT;
                break;
            } else if (xPosition >= horizontalEdge && yPosition >= verticalEdge) {
                enemyDirection = Direction.UP;
                break;
            } else if (yPosition <= 100 && xPosition >= horizontalEdge) {
                enemyDirection = Direction.LEFT;
                break;
            }
        }

        for (int i = 0; i < enemyArrayList.size(); i++) {
            Enemy enemy = enemyArrayList.get(i);
            if (enemyDirection == Direction.UP) {
                enemyArrayList.get(i).setyPosition(enemy.getyPosition() - 10);
            } else if (enemyDirection == Direction.DOWN) {
                enemyArrayList.get(i).setyPosition(enemy.getyPosition() + 10);
            }
            if (enemyDirection == Direction.LEFT) {
                enemyArrayList.get(i).setxPosition(enemy.getxPosition() - 10);
            } else if (enemyDirection == Direction.RIGHT) {
                enemyArrayList.get(i).setxPosition(enemy.getxPosition() + 10);
            }
            enemyArrayList.get(i).updateHitBox();
        }
    }

    public void moveEnemyBulletsToPlayer(Bullet enemyBullet) {

        // 1. calculate distance between bullet and square
        double a = (enemyBullet.getTargetX() - enemyBullet.getxPosition());
        double b = (enemyBullet.getTargetY() - enemyBullet.getyPosition());
        double distance = Math.sqrt((a * a) + (b * b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        enemyBullet.setxPosition(enemyBullet.getxPosition() + (int) (xn * enemyBullet.getSpeed()));
        enemyBullet.setyPosition(enemyBullet.getyPosition() + (int) (yn * enemyBullet.getSpeed()));

        enemyBullet.updateHitBox();

        if (enemyBullet.getHitBox().intersect(player.getHitBox())) {
            enemyPlayerCollided();
            enemyBulletsArrayList.clear();
        } else if (enemyBullet.getxPosition() <= 0 || enemyBullet.getxPosition() >= screenWidth || enemyBullet.getyPosition() <= 0 || enemyBullet.getyPosition() >= screenHeight) {
            enemyBulletsArrayList.remove(enemyBullet);
        } else {
            if (enemyBullet.getxPosition() - enemyBullet.getTargetX() <= 5 || enemyBullet.getyPosition() - enemyBullet.getTargetY() <= 5) {
                enemyBulletsArrayList.remove(enemyBullet);
            }
        }
    }

    public void movePlayerToMouse(float mouseXPos, float mouseYPos) {
        player.setxPosition((int) mouseXPos);
        player.setyPosition((int) mouseYPos);

        player.updateHitBox();
    }

    public void moveBulletToTarget(Bullet bullet) {

        // 1. calculate distance between bullet and square
        double a = (coreEnemy.getxPosition() - bullet.getxPosition());
        double b = (coreEnemy.getyPosition() - bullet.getyPosition());
        double distance = Math.sqrt((a * a) + (b * b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        bullet.setxPosition(bullet.getxPosition() + (int) (xn * bullet.getSpeed()));
        bullet.setyPosition(bullet.getyPosition() + (int) (yn * bullet.getSpeed()));

        bullet.updateHitBox();
    }

    /**
     * Spawning Bullets
     */
    public void spawnBullet() {

        if (enemyArrayList.size() > 0) {
            if (this.bulletsArrayList.size() < 3) {
                createBullets(getContext());
            }
        } else {
            bulletsArrayList.clear();
        }
    }

    public void spawnEnemyBullets() {
        if (enemyArrayList.size() > 0) {
            if (this.enemyBulletsArrayList.isEmpty()) {
                createEnemyBullets(getContext());
            }
        }
    }

    public void spawnShields() {

        if (shouldShieldDisplay) {
            if (shield == null) {
                int x = generateRandomXAxis();
                int y = generateRandomYAxis();

                createShields(getContext(), x, y);
            }
        }
    }

    /**
     * Collision Detection
     */
    public void collisionDetection() {

        // Player - Enemy Collision
        for (Enemy enemy : enemyArrayList) {
            if (player.getHitBox().intersect(enemy.getHitBox())) {
                if (!activateShield)
                totalLives--;
            }
        }

        // Player's Bullet - Enemy
        for (int i = 0; i < bulletsArrayList.size(); i++) {
            Bullet bullet = bulletsArrayList.get(i);
            for (int j = 0; j < enemyArrayList.size(); j++) {
                Enemy enemy = enemyArrayList.get(j);
                if (bullet.getHitBox().intersect(enemy.getHitBox())) {
                    if (enemy == coreEnemy && coreHit < 5) {
                        coreHit++;
                        updateTotalScore(true);
                    } else if (coreHit >= 5) {
                        enemyArrayList.clear();

                    } else {
                        destroyEnemy(bullet, enemy);
                    }
                }
            }
        }
    }

    void enemyPlayerCollided() {
        if (!activateShield)
        totalLives--;
    }

    private void shieldPlayerCollision() {
        if (shield != null && shield.getHitBox() != null) {
            if (player.getHitBox().intersect(shield.getHitBox())) {
                activateShield = true;
                shieldCount = 0;
                shouldShieldDisplay = false;
                shield = null;
            }
        }
    }

    /**
     * Update
     */
    void destroyEnemy(Bullet bullet, Enemy enemy) {

        updateTotalScore(false);
        enemyArrayList.remove(enemy);
        bottomMargin += 10;
        bulletsArrayList.remove(bullet);
    }

    void updateTotalScore(boolean isCoreHit) {
        if (isCoreHit) {
            totalScore += 100;
        } else {
            totalScore += 50;
        }
    }

    int generateRandomXAxis() {

        int min = 100;
        Random r = new Random();
        return r.nextInt((screenWidth/2) - min + 1) + min;
    }

    int generateRandomYAxis() {

        int min = 100;
        Random r = new Random();
        return r.nextInt((screenHeight/2) - min + 1) + min;
    }
}